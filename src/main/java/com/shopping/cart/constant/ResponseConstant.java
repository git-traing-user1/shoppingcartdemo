package com.shopping.cart.constant;

/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/

public class ResponseConstant {

    public static final String SUCCESS_CODE = "200";

    public static final String ERROR_CODE = "400";

    public static final String SUCCESS = "success";

    public static final String ERROR = "error";

    public static final String LOGIN_SUCCESS = "login success";

    public static final String REPEAT_LOGIN = "repeat login";

    public static final String NO_THIS_GOOD = "此商品不存在";

    public static final String NO_EXIST_GOOD_IN_CART = "购物车无商品";

    public static final String GOOD_NUMBER_ERROR = "商品数量不足";
}
