package com.shopping.cart.constant;

/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/

public class ParamExceptionConstants {

    public static final  String USER_ID_NOT_NULL = "用户id不为空";

    public static final  String GOOD_NUMBER_ABOVE_ZERO = "商品数量大于0";

    public static final  String GOOD_ID_NOT_NULL = "商品id不为空";

    public static final  String GOOD_NUMBER_NOT_NULL = "商品数量不为空";
}
