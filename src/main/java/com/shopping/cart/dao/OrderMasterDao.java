package com.shopping.cart.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shopping.cart.model.OrderMaster;
import com.shopping.cart.vo.OrderVo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/

@Repository
public interface OrderMasterDao extends BaseMapper<OrderMaster> {

    /**
     * 查询指定用户的订单信息
     *
     * @param userId 用户id
     * @return 指定用户的订单和订单明细
     */

    @Select("SELECT id,total,create_date,user_id " +
            "FROM order_master " +
            "WHERE user_id = #{userId} " +
            "ORDER BY create_date DESC")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "orderDetailList", column = "id", many = @Many(select = "com.shopping.cart.dao.OrderDetailDao.getOrderDetail"))
    })
    List<OrderVo> getOrderList(@Param("userId")Integer userId);

}
