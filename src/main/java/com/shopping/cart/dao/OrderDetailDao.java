package com.shopping.cart.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shopping.cart.model.OrderDetail;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/

@Repository
public interface OrderDetailDao extends BaseMapper<OrderDetail> {

    /**
     * 批量插入订单明细
     *
     * @param orderDetailList 需要批量插入的订单明细数据
     *
     */

    @Insert("<script>" +
            "INSERT INTO order_detail(order_id,good_name,price,number,order_detail_total) VALUES" +
            "<foreach collection='orderDetailList' item='item' index='index' separator=',' >" +
            "(#{item.orderId},#{item.goodName},#{item.price},#{item.number},#{item.orderDetailTotal})" +
            "</foreach>" +
            "</script>")
    void batchInsertDetail(@Param("orderDetailList") List<OrderDetail> orderDetailList);

    /**
     * 查询订单主表的明细
     *
     * @param id 订单主表id
     * @return 返回主表的订单明细
     */

    @Select("SELECT detail_id,order_id,good_name,price,number,order_detail_total " +
            "FROM order_detail " +
            "WHERE order_id = #{orderId}")
    List<OrderDetail> getOrderDetail(@Param("orderId") Integer id);
}
