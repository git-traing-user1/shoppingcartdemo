package com.shopping.cart.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.shopping.cart.model.Good;
import org.springframework.stereotype.Repository;

/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/

@Repository
public interface GoodDao extends BaseMapper<Good> {


}
