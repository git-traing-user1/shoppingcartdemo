package com.shopping.cart.qo;

import com.shopping.cart.constant.ParamExceptionConstants;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/

@Data
public class AddQo {

    @NotNull(message = ParamExceptionConstants.USER_ID_NOT_NULL)
    Integer userId;

    @NotNull(message = ParamExceptionConstants.GOOD_ID_NOT_NULL)
    Integer goodId;

    @Min(value = 1,message = ParamExceptionConstants.GOOD_NUMBER_ABOVE_ZERO)
    @NotNull(message = ParamExceptionConstants.GOOD_NUMBER_NOT_NULL)
    Integer number;
}
