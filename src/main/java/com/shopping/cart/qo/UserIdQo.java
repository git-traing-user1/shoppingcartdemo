package com.shopping.cart.qo;

import com.shopping.cart.constant.ParamExceptionConstants;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/

@Data
public class UserIdQo {

    @NotNull(message = ParamExceptionConstants.USER_ID_NOT_NULL)
    Integer userId;
}
