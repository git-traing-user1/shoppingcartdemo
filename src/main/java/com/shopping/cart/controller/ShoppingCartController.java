package com.shopping.cart.controller;


import com.shopping.cart.model.PageParam;
import com.shopping.cart.model.Result;
import com.shopping.cart.qo.AddQo;
import com.shopping.cart.qo.DeleteQo;
import com.shopping.cart.qo.UserIdQo;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.shopping.cart.service.ShoppingCartService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/


@RestController
@CrossOrigin(allowCredentials ="true")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ShoppingCartController {


    private final ShoppingCartService shoppingCartService;

    @PostMapping(value = "getGoodList" )
    public Result getGoodList(@RequestBody PageParam pageParam){
        return shoppingCartService.getGoodList(pageParam);
    }


    @PostMapping(value = "getShoppingCart" )
    public Result getShoppingCart(HttpServletRequest request, @RequestBody @Validated UserIdQo userIdQo){
        HttpSession session = request.getSession();
        return shoppingCartService.getShoppingCart(session,userIdQo);
    }

    @PostMapping(value = "addGood" )
    public Result addGood(HttpServletRequest request,@RequestBody @Validated AddQo addQo){
        HttpSession session = request.getSession();
        return shoppingCartService.addGood(session,addQo);
    }

    @PostMapping(value = "deleteGood" )
    public Result addGood(HttpServletRequest request,@RequestBody @Validated DeleteQo deleteQo){
        HttpSession session = request.getSession();
        return shoppingCartService.deleteGood(session,deleteQo);
    }

    @PostMapping(value = "generateOrders")
    public Result generateOrders(HttpServletRequest request,@RequestBody @Validated UserIdQo userIdQo){
        HttpSession session = request.getSession();
        return shoppingCartService.generateOrders(session,userIdQo);
    }

    @PostMapping(value = "getOrdersByUserId")
    public Result getOrders(HttpServletRequest request,@RequestBody @Validated UserIdQo userIdQo){
        HttpSession session = request.getSession();
        return shoppingCartService.getOrdersByUserId(session,userIdQo);
    }

}
