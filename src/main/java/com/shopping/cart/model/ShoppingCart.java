package com.shopping.cart.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;

/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/

@Data
public class ShoppingCart implements Serializable {


    BigDecimal total;

    HashMap<Integer,Good> goods;

    public ShoppingCart(){
        total = BigDecimal.valueOf(0.0);
        goods = new HashMap<>();
    }
}
