package com.shopping.cart.model;

import lombok.Data;

/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/

@Data
public class PageParam {

    Integer pageNum;

    Integer pageSize;
}
