package com.shopping.cart.model;


import com.shopping.cart.constant.ResponseConstant;

/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/

public class Result {


    private String code;

    private String msg;

    private Object data;

    public Result() {}

    public Result(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static Result success() {
        Result Result = new Result();
        Result.setMsg(ResponseConstant.SUCCESS);
        Result.setCode(ResponseConstant.SUCCESS_CODE);
        return Result;
    }

    public static Result success(String msg) {
        Result Result = new Result();
        Result.setMsg(msg);
        Result.setCode(ResponseConstant.SUCCESS_CODE);
        return Result;
    }


    public static Result success(Object data) {
        Result Result = new Result();
        Result.setMsg(ResponseConstant.SUCCESS);
        Result.setCode(ResponseConstant.SUCCESS_CODE);
        Result.setData(data);
        return Result;
    }

    public static Result failure( String msg) {
        Result Result = new Result();
        Result.setCode(ResponseConstant.ERROR_CODE);
        Result.setMsg(msg);
        return Result;
    }

    public static Result failure(String code, String msg, Object data) {
        Result Result = new Result();
        Result.setCode(code);
        Result.setMsg(msg);
        Result.setData(data);
        return Result;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(final String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(final Object data) {
        this.data = data;
    }
}
