package com.shopping.cart.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/

@Data
public class OrderDetail {

    @TableId(type = IdType.AUTO)
    Integer detailId;

    Integer orderId;

    String goodName;

    BigDecimal price;

    Integer number;

    BigDecimal orderDetailTotal;
}
