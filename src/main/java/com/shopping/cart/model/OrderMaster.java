package com.shopping.cart.model;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/

@Data
public class OrderMaster {

    @TableId(type = IdType.AUTO)
    Integer id;

    BigDecimal total;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    Date createDate;

    Integer userId;
}
