package com.shopping.cart.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/

@Data
public class Good {
    @TableId(type = IdType.AUTO)
    Integer id;

    String goodName;

    BigDecimal price;

    Integer number;

}
