package com.shopping.cart.service.impl;

import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import com.google.common.collect.Lists;
import com.shopping.cart.constant.ResponseConstant;
import com.shopping.cart.constant.SessionConstants;
import com.shopping.cart.dao.GoodDao;
import com.shopping.cart.dao.OrderMasterDao;
import com.shopping.cart.dao.OrderDetailDao;
import com.shopping.cart.model.*;
import com.shopping.cart.qo.AddQo;
import com.shopping.cart.qo.DeleteQo;
import com.shopping.cart.qo.UserIdQo;
import com.shopping.cart.vo.OrderVo;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import com.shopping.cart.service.ShoppingCartService;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.shopping.cart.constant.ResponseConstant.NO_EXIST_GOOD_IN_CART;

/**
 * @author:Riaz
 * @date:2020/7/9
 **/
@Service("shoppingCartService")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ShoppingCartServiceImpl implements ShoppingCartService {

    private final GoodDao goodDao;

    private final OrderMasterDao orderMasterDao;

    private final OrderDetailDao orderDetailDao;

    @Override
    public Result getGoodList(PageParam pageParam) {

        PageMethod.startPage(pageParam.getPageNum(), pageParam.getPageSize());
        List<Good> goodList = goodDao.selectList(null);
        PageInfo<Good> pageInfo = new PageInfo<>(goodList);

        return Result.success(pageInfo);
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result addGood(HttpSession session, AddQo addQo) {
        // 获取购物车
        Integer userId = addQo.getUserId();
        ShoppingCart shoppingCart = getCart(session, userId);
        HashMap<Integer, Good> goodMap = shoppingCart.getGoods();
        BigDecimal total = shoppingCart.getTotal();
        Integer goodId = addQo.getGoodId();
        Integer number = addQo.getNumber();

        // 商品检查
        Good good = goodDao.selectById(goodId);
        if (good == null) {
            return Result.failure(ResponseConstant.NO_THIS_GOOD);
        }
        // 商品数量检查
        if (good.getNumber() < number) {
            return Result.failure(good.getGoodName() + ResponseConstant.GOOD_NUMBER_ERROR);
        }


        //添加商品,改变价格
        Good goodInCart = goodMap.get(addQo.getGoodId());
        if (goodInCart == null) {
            good.setNumber(number);
            goodMap.put(good.getId(), good);
            total = total.add(good.getPrice().multiply(BigDecimal.valueOf(number)));
        } else {
            total = total.add(good.getPrice().multiply(BigDecimal.valueOf(Long.valueOf(number) - Long.valueOf(goodInCart.getNumber()))));
            goodInCart.setNumber(number);
        }

        //存入购物车
        ShoppingCart cart = saveShoppingCart(session, total, goodMap, userId);

        return Result.success(cart);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result deleteGood(HttpSession session, DeleteQo deleteQo) {

        // 获取购物车
        Integer userId = deleteQo.getUserId();
        ShoppingCart shoppingCart = getCart(session, deleteQo.getUserId());
        HashMap<Integer, Good> goodMap = shoppingCart.getGoods();
        BigDecimal total = shoppingCart.getTotal();
        Integer goodId = deleteQo.getGoodId();
        Good good = goodDao.selectById(goodId);

        if (good == null) {
            return Result.failure(ResponseConstant.NO_THIS_GOOD);
        }

        // 删除购物车中的商品
        Good goodInCart = goodMap.get(goodId);
        if (goodInCart == null) {
            return Result.failure(NO_EXIST_GOOD_IN_CART);
        } else {
            // 价格减去移除的商品
            total = total.subtract(goodInCart.getPrice().multiply(BigDecimal.valueOf(goodInCart.getNumber())));
            goodMap.remove(goodId);
        }

        //存入购物车
        ShoppingCart cart = saveShoppingCart(session, total, goodMap, userId);

        return Result.success(cart);
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result generateOrders(HttpSession session, UserIdQo userIdQo) {

        // 获取购物车
        ShoppingCart shoppingCart = getCart(session, userIdQo.getUserId());
        HashMap<Integer, Good> goodMap = shoppingCart.getGoods();
        BigDecimal total = shoppingCart.getTotal();
        Integer userId = userIdQo.getUserId();
        if (goodMap.isEmpty()) {
            return Result.failure(NO_EXIST_GOOD_IN_CART);
        }

        // 订单总单
        OrderMaster orderMaster = new OrderMaster();
        orderMaster.setTotal(total);
        orderMaster.setUserId(userId);
        orderMasterDao.insert(orderMaster);
        Integer orderId = orderMaster.getId();

        // 处理订单明细单
        List<OrderDetail> orderDetailList = Lists.newArrayList();
        BeanCopier beanCopier = BeanCopier.create(Good.class, OrderDetail.class, false);

        for (Map.Entry<Integer, Good> entry : goodMap.entrySet()) {
            // 对象赋值
            Good good = entry.getValue();
            OrderDetail tempOderDetail = new OrderDetail();

            beanCopier.copy(good, tempOderDetail, null);
            tempOderDetail.setOrderId(orderId);
            tempOderDetail.setOrderDetailTotal(good.getPrice().multiply(BigDecimal.valueOf(good.getNumber())));
            orderDetailList.add(tempOderDetail);
        }

        // 批量插入订单明细
        orderDetailDao.batchInsertDetail(orderDetailList);

        session.removeAttribute(SessionConstants.SESSION_NAME + userId);

        return Result.success();
    }

    @Override
    public Result getOrdersByUserId(HttpSession session, UserIdQo userIdQo) {

        List<OrderVo> orderVoList = orderMasterDao.getOrderList(userIdQo.getUserId());

        return Result.success(orderVoList);
    }


    private ShoppingCart getCart(HttpSession session, Integer userId) {
        ShoppingCart shoppingCart = (ShoppingCart) session.getAttribute(SessionConstants.SESSION_NAME + userId);
        if (shoppingCart == null) {
            return new ShoppingCart();
        }
        return shoppingCart;
    }


    @Override
    public Result getShoppingCart(HttpSession session, UserIdQo userIdQo) {

        // 获取购物车
        ShoppingCart shoppingCart = getCart(session, userIdQo.getUserId());

        return Result.success(shoppingCart);
    }


    private ShoppingCart saveShoppingCart(HttpSession session, BigDecimal total, HashMap<Integer, Good> goodMap, Integer userId) {
        ShoppingCart cart = new ShoppingCart();
        cart.setTotal(total);
        cart.setGoods(goodMap);

        session.setAttribute(SessionConstants.SESSION_NAME + userId, cart);
        return cart;
    }

}
