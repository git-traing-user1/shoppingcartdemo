package com.shopping.cart.service;

import com.shopping.cart.model.PageParam;
import com.shopping.cart.model.Result;
import com.shopping.cart.qo.AddQo;
import com.shopping.cart.qo.DeleteQo;
import com.shopping.cart.qo.UserIdQo;

import javax.servlet.http.HttpSession;

/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/


public interface ShoppingCartService {

    /**
     * 分页查询商品列表
     *
     * @param pageParam 分页参数
     * @return 返回商品列表
     */

    Result getGoodList(PageParam pageParam);

    /**
     * 获取购物车
     *
     * @param session 会话
     * @param userIdQo 用户id参数
     * @return 返回购物车
     */

    Result getShoppingCart(HttpSession session, UserIdQo userIdQo);

    /**
     * 向指定id用户的购物车添加或者修改商品
     *
     * @param session 会话
     * @param addQo 添加到购物车的商品信息和用户id
     * @return 返回操作结果
     */

    Result addGood(HttpSession session, AddQo addQo);

    /**
     * 向指定id用户的购物车移除商品
     *
     * @param session 会话
     * @param deleteQo 用户id和要删除的商品信息
     * @return 返回操作结果
     */

    Result deleteGood(HttpSession session, DeleteQo deleteQo);

    /**
     * 生成指定用户购物车里的商品订单
     *
     * @param session 会话
     * @param userIdQo 用户id
     * @return 返回操作结果
     */

    Result generateOrders(HttpSession session,UserIdQo userIdQo);

    /**
     * 通过用户id获取订单信息
     *
     * @param session 会话
     * @param userIdQo 用户id
     * @return 返回订单
     */

    Result getOrdersByUserId(HttpSession session,UserIdQo userIdQo);
}
