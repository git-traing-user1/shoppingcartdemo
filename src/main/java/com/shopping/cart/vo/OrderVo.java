package com.shopping.cart.vo;

import com.shopping.cart.model.OrderDetail;
import com.shopping.cart.model.OrderMaster;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/

@Data()
@EqualsAndHashCode(callSuper = true)
public class OrderVo extends OrderMaster {

    List<OrderDetail> orderDetailList;
}
